<?php include_once 'inc/top.php';?>

<?php

try {
    $id=filter_input(INPUT_GET,'id',FILTER_SANITIZE_NUMBER_INT);
    $sql = "DELETE FROM asiakas WHERE id=$id";
    
    $tietokanta->exec($sql);

    print '<p>Asiakas poistettu rekisteristä.</p>';
    print "<br /><a href=index.php>Takaisin etusivulle.</a>";

    } catch (Exception $pdoex) {
            print "Poistossa tapahtui virhe <br />" . $pdoex->getMessage();
    }
?>
<?php include_once 'inc/bottom.php';?>