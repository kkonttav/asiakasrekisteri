<?php include_once 'inc/top.php';?>

<?php
if (!isset($_SESSION['kirjautunut'])) {
?>
    <h3>Asiakasrekisterin näkeminen vaatii kirjautumisen.</h3>
    <form role="form" action='tarkasta.php' method="post">
      <div class="form-group">
        <label for="tunnus">Käyttäjätunnus:</label>
        <input type="text" class="form-control" name="tunnus" size="30" maxlength="30" autofocus="">
      </div>
      <div class="form-group">
        <label for="salasana">Salasana:</label>
        <input type="password" class="form-control" name="salasana" size="30" maxlength="30">
      </div>
      <button type="submit" class="btn btn-primary">Kirjaudu</button>
      <a type=btn class="btn btn-primary" href="kirjaudu.php">Lisää käyttäjä</a>
    </form> 
<?php
}
else
{
?>
<div class="container">
    
        <div class="col-xs-4">
            <form class="navbar-form navbar-left" role="search" action="<?php print($_SERVER['PHP_SELF']);?>" method="post">
                <div class="form-group">
                    <input name="etsi" type="text" class="form-control" placeholder="Search" autofocus>
                    <button type="submit" class="btn btn-default btn-sm"><span class="glyphicon glyphicon-search"></span></button>
                    <a type=btn class="btn btn-primary" href="tallenna.php">Lisää</a>
                </div>
            </form>
        </div>
        <form role="form" action="<?php print($_SERVER['PHP_SELF']);?>" method="post">
            <div class="col-xs-6">
                <div class="form-group">
                        <select name="suorita" class="form-control">
                            <option value="0">Valitse</option>                          
                            <option value="1">Poista</option>
                        </select> 
                    <button type="submit" class="btn btn-default btn-sm">Suorita</button>
                </div>
            </div>
            <div class="col-xs-12">
                
<?php
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    /* Search kenttän arvo */
    $like=filter_input(INPUT_POST,'etsi',FILTER_SANITIZE_STRING);
    /* Suorita valnnan arvo */
    $valinta=filter_input(INPUT_POST,'suorita',FILTER_SANITIZE_NUMBER_INT);
    
    if($valinta != 0) {
        if (isset($_POST['valinta_lista'])) {
            /* Suorita nappia painettu */
            $lista = $_POST['valinta_lista'];
            $sql="DELETE FROM asiakas WHERE id IN (";
            if(!empty($lista)) {
                foreach($lista as $id)
                    $sql= $sql . $id . ",";
                $sql2 = rtrim($sql, ",");
                $sql2= $sql2 . ")";
                print "$sql2<br/>";
                }
                try {
                    $tietokanta->exec($sql2);
                    } catch (Exception $pdoex) {
                            print "Poistossa tapahtui virhe <br />" . $pdoex->getMessage();
                    }          
            }
        }
    
    $sql='SELECT * FROM asiakas';
    if( strlen($like)>0) {
        $sql.=" WHERE sukunimi like '$like%'";
        }
}
else {
       $order=filter_input(INPUT_GET,'jarjesta',FILTER_SANITIZE_STRING);
       if($order !=null) { /* Sarakkeen otsikko painettu --> järjestä rivit */
           $order=filter_input(INPUT_GET,'jarjesta',FILTER_SANITIZE_STRING);
           $sql= "SELECT * FROM asiakas ORDER BY $order";           
       }
       else { /* Lue normaalisti tiedot */
           $sql='SELECT * FROM asiakas';    
       }
}
    try {
        $kysely=$tietokanta->query($sql);
        $kysely->setFetchMode(PDO::FETCH_OBJ);

        print "<table class='table'>";
        print "<thead>";
        print " <tr>";
        print "  <th><a href='index.php?jarjesta=sukunimi,etunimi'>Sukunimi</a></th>";
        print "  <th><a href='index.php?jarjesta=etunimi,sukunimi'>Etunimi</a></th>";
        print "  <th><a href='index.php?jarjesta=lahiosoite,sukunimi,etunimi'>Lähiosoite</a></th>";
        print "  <th><a href='index.php?jarjesta=postinumero,sukunimi,etunimi'>Postinumero</a></th>";
        print "  <th><a href='index.php?jarjesta=postitoimipaikka,sukunimi,etunimi'>Postitoimipaikka</a></th>";
        print "  <th>Muokkaa</th>";
        print "  <th>Poista</th>";
        print "  <th>Muistio</th>";
        print "  <th>Valitse</th>";
        print " </tr>";
        print "</thead>";
        print "<tbody>";
        while($tietue  =$kysely->fetch()) {
            print '<tr>';
            print '<td>' . $tietue->sukunimi . '</td>';
            print '<td>' . $tietue->etunimi . '</td>';
            print '<td>' . $tietue->lahiosoite . '</td>';
            print '<td>' . $tietue->postinumero . '</td>';
            print '<td>' . $tietue->postitoimipaikka . '</td>';
            print '<td>';
            ?>        
           <a href="tallenna.php?id=<?php print "$tietue->id"?>";><span class="glyphicon glyphicon-pencil"></span>
            <?php
            print '</td>';            
            print '<td>';
            ?>        
           <a href="poista.php?id=<?php print "$tietue->id"?>";><span class="glyphicon glyphicon-trash"></span>
           <?php
            print '</td>';
            print '<td>';
            ?>        
           <a href="muistio.php?id=<?php print "$tietue->id"?>";><span class="glyphicon glyphicon-file"></span>
           <?php
            print '</td>';
            print '<td>';
            ?>
                <input type="checkbox" name="valinta_lista[]" value="<?php print "$tietue->id"?>">
           <?php           
           print '</tr>';
        }
        print "</tbody>";
        print "</table>";
        print '<p>Asiakkaita: ' . $kysely->rowCount() . '</p>';

    } catch (Exception $pdoex) {
            print "Luennassa tapahtui virhe" . $pdoex->getMessage();
    }
}
?>
            </div>
        </form>
</div>                
                
<?php include_once 'inc/bottom.php';?>