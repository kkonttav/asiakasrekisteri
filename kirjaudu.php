<?php include_once 'inc/top.php';?>

<?php
    if($_SERVER["REQUEST_METHOD"] == "POST")
        {
        try {
            $tunnus=filter_input(INPUT_POST,'tunnus',FILTER_SANITIZE_STRING);
            $salasana=md5(filter_input(INPUT_POST,'salasana',FILTER_SANITIZE_STRING));
            $sahkoposti=filter_input(INPUT_POST,'sahkoposti',FILTER_SANITIZE_STRING);
            
            $kysely = $tietokanta->prepare("INSERT INTO kayttaja (tunnus,salasana,sahkoposti) "
                    . "VALUES (:tunnus,:salasana,:sahkoposti)");
            $kysely->bindValue(':tunnus', $tunnus, PDO::PARAM_STR);
            $kysely->bindValue(':salasana', $salasana, PDO::PARAM_STR);
            $kysely->bindValue(':sahkoposti', $sahkoposti, PDO::PARAM_STR);
            $kysely->execute();
            print "<p>Käyttäjä liitettiin rekisteriin.</p>";
        } catch (PDOException $pdoex) {
                print "Tallenneuksessa tapahtui virhe. <br />" . $pdoex->getMessage();
        }
        print "<a href=index.php>Takaisin etusivulle.</a>";        
    }
    else {
?>        
    <h3>Talleta uusi käyttäjä.</h3>
    
    <form role="form" action='<?php print($_SERVER['PHP_SELF']); ?>' method="post">
      <div class="form-group">
        <label for="tunnus">Tunnus:</label>
        <input type="text" class="form-control" name="tunnus"  autofocus>
      </div>
      <div class="form-group">
        <label for="salasana">Salasana:</label>
        <input type="password" class="form-control" name="salasana">
      </div>
      <div class="form-group">
        <label for="sahkoposti">Sähköposti:</label>
        <input type="text" class="form-control" name="sahkoposti">
      </div>
      <button type="submit" class="btn btn-primary">Tallenna</button>
      <input type="button" class="btn btn-default" onclick="window.location='index.php';return false;" value='Peruuta'>
    </form> 
<?php
    }
?>