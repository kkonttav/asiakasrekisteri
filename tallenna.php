<?php include_once 'inc/top.php';?>

<?php 
    $luettusukunimi = "";
    $luettuetunimi = "";
    $luettulahiosoite = "";
    $luettupostinumero = "";
    $luettupostitoimipaikka = "";
    $luettuid = 0;
    if($_SERVER["REQUEST_METHOD"] == "POST")
        {
        try {
            $sukunimi=filter_input(INPUT_POST,'sukunimi',FILTER_SANITIZE_STRING);
            $etunimi=filter_input(INPUT_POST,'etunimi',FILTER_SANITIZE_STRING);
            $lahiosoite=filter_input(INPUT_POST,'lahiosoite',FILTER_SANITIZE_STRING);
            $postinumero=filter_input(INPUT_POST,'postinumero',FILTER_SANITIZE_STRING);
            $postitoimipaikka=filter_input(INPUT_POST,'postitoimipaikka',FILTER_SANITIZE_STRING);
            $id=filter_input(INPUT_POST,'id',FILTER_SANITIZE_NUMBER_INT);
            
            if($id ==0) {

                $kysely = $tietokanta->prepare("INSERT INTO asiakas (sukunimi,etunimi,lahiosoite, postinumero, postitoimipaikka) "
                        . "VALUES (:sukunimi,:etunimi,:lahiosoite,:postinumero,:postitoimipaikka)");
                $kysely->bindValue(':sukunimi', $sukunimi, PDO::PARAM_STR);
                $kysely->bindValue(':etunimi', $etunimi, PDO::PARAM_STR);
                $kysely->bindValue(':lahiosoite', $lahiosoite, PDO::PARAM_STR);
                $kysely->bindValue(':postinumero', $postinumero, PDO::PARAM_STR);
                $kysely->bindValue(':postitoimipaikka', $postitoimipaikka, PDO::PARAM_STR);
                $kysely->execute();
                print "<p>Asiakas liitettiin rekisteriin.</p>";
            }
            else
            {
                $sql="UPDATE asiakas SET sukunimi='$sukunimi', etunimi='$etunimi', lahiosoite='$lahiosoite', postinumero='$postinumero', postitoimipaikka='$postitoimipaikka' WHERE id = $id";
                $tietokanta->query($sql);    
                print "<p>Asiakas päivitetty rekisteriin.</p>";
            }
            print "<a href=index.php>Takaisin etusivulle.</a>";
        } catch (PDOException $pdoex) {
                print "Tallenneuksessa tapahtui virhe. <br />" . $pdoex->getMessage();
        }
        
        }
    else if($_SERVER["REQUEST_METHOD"] == "GET")
    {
        $id=filter_input(INPUT_GET,'id',FILTER_SANITIZE_NUMBER_INT);
        if($id != 0) {
            $sql= "SELECT * FROM asiakas WHERE ID =$id";
            $kysely=$tietokanta->query($sql);
            $kysely->setFetchMode(PDO::FETCH_OBJ);

            $tietue  =$kysely->fetch();

            $luettusukunimi = $tietue->sukunimi;
            $luettuetunimi = $tietue->etunimi;
            $luettulahiosoite = $tietue->lahiosoite;
            $luettupostinumero = $tietue->postinumero;
            $luettupostitoimipaikka = $tietue->postitoimipaikka;
            $luettuid =$tietue->id;
        }
    }
    
    ?>
    <h3>Ilmoittautuminen</h3>
    <form role="form" action='<?php print($_SERVER['PHP_SELF']); ?>' method="post">
      <div class="form-group">
        <label for="sukunimi">Sukunimi:</label>
        <input type="text" class="form-control" name="sukunimi" value='<?php print"$luettusukunimi"; ?>' autofocus>
      </div>
      <div class="form-group">
        <label for="etunimi">Etunimi:</label>
        <input type="text" class="form-control" name="etunimi" value='<?php print"$luettuetunimi"; ?>'>
      </div>
      <div class="form-group">
        <label for="lahiosoite">Lähiosoite:</label>
        <input type="text" class="form-control" name="lahiosoite" value='<?php print"$luettulahiosoite"; ?>'>
      </div>
      <div class="form-group">
        <label for="postinumero">Postinumero:</label>
        <input type="text" class="form-control" name="postinumero" value='<?php print"$luettupostinumero"; ?>'>
      </div>
      <div class="form-group">
        <label for="postitoimipaikka">Postitoimipaikka:</label>
        <input type="text" class="form-control" name="postitoimipaikka" value='<?php print"$luettupostitoimipaikka"; ?>'>
      </div>    
      <div class="form-group">
        <label for="id"></label>
        <input type="hidden" class="form-control" name="id" value='<?php print"$luettuid"; ?>'>
      </div>            
      <button type="submit" class="btn btn-primary">Tallenna</button>
      <input type="button" class="btn btn-default" onclick="window.location='index.php';return false;" value='Peruuta'>
    </form>        
    
<?php include_once 'inc/bottom.php';?>