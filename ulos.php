<?php
session_start();
session_regenerate_id();
?>

<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        unset($_SESSION['kirjautunut']);
        session_destroy();
        header("location: index.php");
        ?>
    </body>
</html>
