    drop database if exists asiakasrekisteri;

    create database asiakasrekisteri;

    use asiakasrekisteri;

    create table asiakas (
    id int primary key auto_increment,
    sukunimi varchar(50) not null,
    etunimi varchar(50) not null,
    lahiosoite varchar(50) not null,
    postinumero char(5) not null,
    postitoimipaikka varchar(30) not null
    );

    create table kayttaja (
    id int primary key auto_increment,
    tunnus varchar(30) not null,
    salasana varchar(50) not null,
    sahkoposti varchar(50) not null
    );

    create table muistiinpano (
    id int primary key auto_increment,
    teksti text,
    paivays timestamp default current_timestamp
    on update current_timestamp,
    asiakas_id int not null,
    index (asiakas_id),
    foreign key (asiakas_id) references asiakas(id) on delete restrict
    );