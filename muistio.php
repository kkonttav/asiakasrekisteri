<?php include_once 'inc/top.php';?>

<?php
    $id=0;
    $poistoid=0;
    
    if($_SERVER["REQUEST_METHOD"] == "GET") {
        $id=filter_input(INPUT_GET,'id',FILTER_SANITIZE_NUMBER_INT);
        $poistoid=filter_input(INPUT_GET,'poisto_id',FILTER_SANITIZE_NUMBER_INT);
        if ($poistoid != 0)
            {
            try {
                $sql= "DELETE FROM muistiinpano WHERE ID =$poistoid";
                $tietokanta->exec($sql);
            } catch (PDOException $pdoex) {
                print "Muistion poistossa tapahtui virhe. <br />" . $pdoex->getMessage();
                }
            }
        }
    else if($_SERVER["REQUEST_METHOD"] == "POST")
        {
        $muistiinpano=filter_input(INPUT_POST,'muistiinpano',FILTER_SANITIZE_STRING);
        $id=filter_input(INPUT_POST,'id',FILTER_SANITIZE_NUMBER_INT);
        try {
            $kysely = $tietokanta->prepare("INSERT INTO muistiinpano (teksti, asiakas_id) "
                    . "VALUES (:teksti, :asiakas_id)");
            $kysely->bindValue(':teksti', $muistiinpano, PDO::PARAM_STR);
            $kysely->bindValue(':asiakas_id', $id, PDO::PARAM_INT);
            $kysely->execute();
        } catch (PDOException $pdoex) {
            print "Tallenneuksessa tapahtui virhe. <br />" . $pdoex->getMessage();
            }
        }
    if($id != 0) 
        {
        try {
            $sql= "SELECT * FROM asiakas WHERE ID =$id";
            $kysely=$tietokanta->query($sql);
            $kysely->setFetchMode(PDO::FETCH_OBJ);
            $tietue  =$kysely->fetch();
            $luettusukunimi = $tietue->sukunimi;
            $luettuetunimi = $tietue->etunimi;
            $luettuid =$tietue->id;        
        } catch (PDOException $pdoex) {
            print "Tietokannassa tapahtui virhe. <br />" . $pdoex->getMessage();            
            }
        }
    ?>
    <h3><?php print"$luettusukunimi , $luettuetunimi"; ?></h3>
    <a href=index.php>Takaisin etusivulle.</a>
    <form role="form" action='<?php print($_SERVER['PHP_SELF']); ?>' method="post">
      <div class="form-group">
        <label for="sukunimi">Muistiinpanot:</label>
        <textarea type="text" class="form-control" name="muistiinpano"></textarea>
      </div>
      <button type="submit" class="btn btn-primary">Tallenna</button>
      <div class="form-group">
        <label for="id"></label>
        <input type="hidden" class="form-control" name="id" value='<?php print"$luettuid"; ?>'>
      </div>            
    </form>        
    
<?php

    try {
        $sql="SELECT * FROM muistiinpano WHERE asiakas_id=$id ORDER BY paivays DESC";
        $kysely=$tietokanta->query($sql);
        $kysely->setFetchMode(PDO::FETCH_OBJ);

        while($tietue=$kysely->fetch()) {
            print "<div><br />";
            print date('d.m.Y H:i:s', strtotime($tietue->paivays));
            ?>
            <a href="muistio.php?id=<?php print "$id"?>&poisto_id=<?php print "$tietue->id"?>";><span class="glyphicon glyphicon-trash"></span></a>
            <?php
            print "<br />";
            print $tietue->teksti;
            print "</div>";
            }
    } catch (Exception $pdoex) {
            print "Luennassa tapahtui virhe" . $pdoex->getMessage();
    }
?>
    
<?php include_once 'inc/bottom.php';?>